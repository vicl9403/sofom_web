<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');

            //Relaciones
            $table->integer('user_id');
            $table->integer('room_id');

            // Valores necesarios para el calendario
            $table->string('title');
            $table->boolean('all_day')->default(0);
            $table->dateTime('start');
            $table->dateTime('end');
            $table->string('url',200)->default('');
            $table->string('class_name')->default('');
            $table->boolean('editable')->default(0);
            $table->boolean('start_editable')->default(0);
            $table->boolean('duration_editable')->default(0);
            $table->boolean('resource_editable')->default(0);
            $table->string('color')->default('#32C4D1');
            $table->string('background_color')->default('#32C4D1');
            $table->string('border_color')->default('#0000FF');  
            $table->string('text_color')->default("#FFF");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
