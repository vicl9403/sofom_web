@extends('layouts.website')

@section('title', 'Page Title')

@section('navbar')
    @parent
@endsection

@section('content')

    <div class="container">


        <div class="row">

            <div class="col-md-12">

                <h4>Buró de identidades Financieras</h4>
                <br>

                <a href="https://www.buro.gob.mx/tbl_general_comportamiento2.php?id_sector=69&id_periodo=5#."
                   target="_blank"
                >
                    <img src="/buro.jpg" alt="">
                </a>

                <br>

                <p >
                    Es una herramienta de consulta y difusión con la que podrás conocer los productos que ofrecen las entidades
                    financieras, sus comisiones y tasas, las reclamaciones de los usuarios, las prácticas no sanas en que incurren,
                    las sanciones administrativas que les han impuesto, las cláusulas abusivas de sus contratos y otra información
                    que resulte relevante para informarte sobre su desempeño.
                    <br>

                    Con el Buró de Entidades Financieras, se logrará saber quién es quién en bancos, seguros, sociedades financieras
                    de objeto múltiple, cajas de ahorro, afores, entre otras entidades. <br>

                    Con ello, podrás comparar y evaluar a las entidades financieras, sus productos y servicios y tendrás mayores
                    elementos para elegir lo que más te convenga. <br>

                    Esta información te será útil para elegir un producto financiero y también para conocer y usar mejor los que ya tienes.
                    <br>

                    Este Buró de Entidades Financieras, es una herramienta que puede contribuir al crecimiento económico del país,
                    al promover la competencia entre las instituciones financieras; que impulsará la transparencia al revelar
                    información a los usuarios sobre el desempeño de éstas y los productos que ofrecen y que va a facilitar un
                    manejo responsable de los productos y servicios financieros al conocer a detalle sus características.
                    <br>

                    Lo anterior, podrá derivar en un mayor bienestar social, porque al conjuntar en un solo espacio tan diversa
                    información del sistema financiero, el usuario tendrá más elementos para optimizar su presupuesto, para mejorar
                    sus finanzas personales, para utilizar correctamente los créditos que fortalecerán su economía y obtener los
                    seguros que la protejan, entre otros aspectos. <br>

                    La información contenida, corresponde únicamente a Corporativo Financiero Vimifos SA de CV SOFOM ENR. Para conocer la información de todo el sector, puede acceder al sitio
                    <a href="http://www.buro.gob.mx" target="_blank">http://www.buro.gob.mx</a>
                    <br>
                </p>

            </div>


        </div>
    </div>

@endsection



@section('footer')
    @parent
@endsection

@section('scripts')



@endsection


