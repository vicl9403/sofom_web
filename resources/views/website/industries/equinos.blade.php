@extends('layouts.website')

@section('title', 'Page Title')

@section('navbar')
    @parent
@endsection

@section('content')
	
	<div class="container">

		
		<div class="row">
			
			<div class="col-md-12">

				<!-- Tabs login -->
				<ul class="nav nav-tabs tab-bovino" role="tablist">
					<li role="presentation" class="active">
						<a href="#bovino" aria-controls="bovino" role="tab" data-toggle="tab">EQUINOS</a>
					</li>
					<li role="presentation">
						<a href="#introduccion" aria-controls="introduccion" role="tab" data-toggle="tab">INTRODUCCIÓN</a>
					</li>
					<li role="presentation">
						<a href="#productos" aria-controls="productos" role="tab" data-toggle="tab">PRODUCTOS</a>
					</li>
					<li role="presentation">
						<a href="#tecnologias" aria-controls="tecnologias" role="tab" data-toggle="tab">TECNOLOGÍAS</a>
					</li>
					<li role="presentation">
						<a href="#alianzas" aria-controls="alianzas" role="tab" data-toggle="tab">ALIANZAS</a>
					</li>
				</ul>

				<!-- Contenido de las tabs login -->
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active padding-5 " id="bovino">
						<div class="row margin-top-15">
							<div class="col-md-12">
								<img class="center-block" src="/img/industrias/equinos/equinos.jpg" alt="">
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="introduccion">
						<div class="row margin-top-15">
							<div class="col-md-8">
								<h3 class="bovino-color">INTRODUCCIÓN</h3> <br>
								<p class="text-justify bovino-color">
									<b>En la División de Equinos estamos dedicados a ofrecer los mejores productos, pues sabemos que no es sólo un caballo, es tú equipo, tú compañero.</b> 
								</p>
								<br>
								<p class="text-justify">
									El nuevo programa de nutrición equina de VIMIFOS combina el uso de forrajes de buena calidad, productos vitamínicos-minerales y alimentos fortificados para generar raciones integrales bien balanceadas. Con esta combinación de factores, los caballos permanecen sanos y pueden desarrollar su mejor comportamiento, recibiendo una cantidad mínima de grano en comparación con los sistemas de alimentación convencionales.
 									<br><br>
									En consecuencia, los caballos tienen menor riesgo de presentar trastornos metabólicos, digestivos y ortopédicos, problemas que se presentan con frecuencia en casos de consumo elevado de granos. Todos los productos están elaborados en las dosis necesarias para cubrir los requerimientos de cada fase de alimentación, ofreciendo las siguientes ventajas:
									<br><br> 
									El paquete de productos y servicios disponibles para Caballos están soportados por VIMIFOS con un excelente servicio técnico en las áreas de nutrición, manejo, sanidad, aseguramiento de calidad, reproducción y análisis de la información , así como  también por asesores externos tanto nacionales como extranjeros.
									<br><br>
									<b class="bovino-color">Primero forraje:</b><br>
									Nuestra filosofía nutricional única, Forraje Primero, está específicamente diseñada para alimentar a su caballo de forma fácil y de acuerdo a su naturaleza.
								</p>
								
								
							</div>
							<div class="col-md-4">
								<img src="/img/industrias/bovino/introduccion.png"  class="pull-right" alt="">
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="productos">
						<div class="row margin-top-15">
							<div class="col-md-8">
								
								<div class="row margin-top-15">
									<div class="col-md-3">
										<img src="/img/industrias/equinos/productos/1.png" class="center-block" alt="">
									</div>
									<div class="col-md-9">
										<p class="text-justify">
											Alimento peletizado, muy palatable, bajo en almidón y fortificado con vitaminas y minerales. Cumpliendo con los requerimientos de proteína, energía, vitaminas y minerales.<br>
											<b class="bovino-color">Alimento alto en fibra de alto desempeño.</b>
										</p>
									</div>
								</div>
								<div class="row margin-top-15">
									<div class="col-md-3">
										<img src="/img/industrias/equinos/productos/2.png" class="center-block" alt="">
									</div>
									<div class="col-md-9">
										<p class="text-justify">
											Ofrece las vitaminas y minerales necesarios para complementar el aporte de los forrajes y granos, permitiéndoles a los caballos alcanzar su máximo potencial de rendimiento. <br>
											<b class="bovino-color">Suplemento mineral y vitamínico de alto desempeño.</b>
										</p>
									</div>
								</div>
								<div class="row margin-top-15">
									<div class="col-md-3">
										<img src="/img/industrias/equinos/productos/3.png" class="center-block" alt="">
									</div>
									<div class="col-md-9">
										<p class="text-justify">
											Formulado a base de los mejores ingredientes, para que tu caballo obtenga el mejor rendimiento.<br>
											<b class="bovino-color">Alimento Multiparticula balanceado de alto desempeño. </b>
										</p>
									</div>
								</div>		

								<div class="row">
									<img src="/img/industrias/equinos/productos/4.png" class="center-block" alt="">
								</div>			
								
								
							</div>
							<div class="col-md-4">
								<img src="/img/industrias/equinos/productos.png"  class="img-100" alt="">
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="tecnologias">
						<div class="row margin-top-15">
							<div class="col-md-8">
								
								<h5 class="bovino-color">TECNOLOGÍAS</h5>
								<br>
								<p class="text-justify bovino-color">
									<b>
									Una de las grandes cualidades que diferencian a VIMIFOS es la utilización de tecnología exclusiva aplicada al desarrollo de la nutrición.
									</b>
								</p>

								<li class="bovino-color">Citristim:</li>
								<p class="text-justify">
									Oligasacarido de mananos (MOS) es un producto de levadura propietario de ADM que puede ser utilizado en todas las etapas de producción de ganado de leche. Los digasacáridos manamos han demostrado poder ser utilizados como secuestrantes de bacterias patógenas incluyendo E. Cali, Salmonelia, Proteus, Klebsuela y Clastridium.
								</p>

								<br><br>

								<li class="bovino-color">Vitamina E: </li>
								<p class="text-justify">
									Importante antioxidante soluble en lípidos-redundante, que protege a las lipoproteínas y los ácidos grasos insaturados en las membranas celulares. La lucha contra los efectos de los radicales libres derivados del oxígeno y los contaminantes del medio ambiente, la vitamina E reduce la tasa de ataque de los radicales libres sobre los ácidos grasos poliinsaturados en la membrana de fosfolípidos y en otros sitios de importancia biológica.
								</p>

								<div class="row">	
									<div class="col-md-6">
										<img src="/img/industrias/bovino/tecnologia/1.png" class="center-block" alt="">
									</div>
									<div class="col-md-6 margin-top-30">
										<img src="/img/industrias/equinos/tecnologias-1.png" class="center-block" alt="">
									</div>
								</div>
								

								
							</div>
							<div class="col-md-4">
								<img src="/img/industrias/equinos/tecnologias.png"  class="img-100 pull-right" alt="">
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="alianzas">
						<div class="row margin-top-15">
							<div class="col-md-8">
								
								<h5 class="bovino-color">ALIANZAS</h5>
								<br>
								<p class="text-justify bovino-color">
									<b>
									Contamos con alianzas estratégicas que nos apoyan a elaborar productos únicos que ayudan a obtener una mayor productividad en los negocios de nuestros clientes.
									</b>
								</p>

								<li class="bovino-color">ADM:</li>
								<p class="text-justify">
									Archer Daniels Midland Company es uno de los más grandes procesadores agrícolas del mundo. Fundada en 1902 e incorporada en 1923. ADM tiene su sede en DECATUR, su operación es en todo el mundo a través de sus amplias instalaciones de distribución global. <br><br>
									ADM hace una contribución significativa a la economía mundial y la calidad de vida.
								</p>

								<img src="/img/industrias/porcicultura/adm.png" class="center-block margin-top-15" alt="">
							</div>
							<div class="col-md-4">
								<img src="/img/industrias/porcicultura/alianzas.png"  class="img-responsive pull-right" alt="">
							</div>
						</div>
					</div>
				</div>

			</div>
			

		</div>
	</div>

@endsection



@section('footer')
	@parent
@endsection

@section('scripts')
	
	

@endsection


