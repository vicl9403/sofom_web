@extends('layouts.website')

@section('title', 'Page Title')

@section('navbar')
    @parent
@endsection

@section('content')
	
	<div class="container">

		
		<div class="row">
			
			<div class="col-md-12">

				<!-- Tabs login -->
				<ul class="nav nav-tabs tab-porcicultura" role="tablist">
					<li role="presentation" class="active">
						<a href="#avicultura" aria-controls="avicultura" role="tab" data-toggle="tab">PORCICULTURA</a>
					</li>
					<li role="presentation">
						<a href="#introduccion" aria-controls="introduccion" role="tab" data-toggle="tab">INTRODUCCIÓN</a>
					</li>
					<li role="presentation">
						<a href="#productos" aria-controls="productos" role="tab" data-toggle="tab">PRODUCTOS</a>
					</li>
					<li role="presentation">
						<a href="#tecnologias" aria-controls="tecnologias" role="tab" data-toggle="tab">TECNOLOGÍAS</a>
					</li>
					<li role="presentation">
						<a href="#alianzas" aria-controls="alianzas" role="tab" data-toggle="tab">ALIANZAS</a>
					</li>
				</ul>

				<!-- Contenido de las tabs login -->
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active padding-5 " id="avicultura">
						<div class="row margin-top-15">
							<div class="col-md-12">
								{{-- <img class="center-block" src="/img/industrias/avicultura/tab-1.png" alt=""> --}}
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="introduccion">
						<div class="row margin-top-15">
							<div class="col-md-8">
								<h3 class="porcicultura-color">INTRODUCCIÓN</h3> <br>
								<p class="text-justify porcicultura-color">
									<b>El objetivo de VIMIFOS en la porcicultura es proporcionar productos y servicios de la más alta calidad que permitan a nuestros clientes alcanzar sus objetivos de producción con la mayor eficiencia posible.</b> 
								</p>
								<br>
								<p class="text-justify">
									Nuestros especialistas en producción porcina y nutrición animal desarrollan planes estratégicos de alimentación mediante la combinación de nuestras diferentes líneas de productos y la formulación específica de acuerdo a los insumos y características de cada granja con la finalidad de ofrecer la seguridad, presición y la economía necesaria para garantizar la productividad de acuerdo a las exigencias de las genéticas modernas y los sistemas de producción. <br><br>
									Todos los productos están elaborados en las dósis necesarias para cubrir requerimientos de cada fase de alimentación y su identificación por colores nos ofrece las siguientes ventajas
								</p>
								<br>
								<ul>
									<li class="porcicultura-color">Seguridad</li>
									<p>Disminuye la confusión y facilita el manejo del inventario por la fácil identificación.</p>
									<li class="porcicultura-color">Presición</li>
									<p>Dósis completas permiten la facilidad de operación en las plantas de alimentos.</p>
									<li class="porcicultura-color">Economía</li>
									<p>Su diseño y manejo de dósis exactas minimizan el desperdicio de microingredientes</p>
									<br>
									<p class="text-justify">Mayor cantidad de fases reduce costo de alimentación y permiten cubrir con mayor exactitud los requerimientos nutricionales del cerdo en distintas fases de crecimiento y producción</p>
								</ul>
								<br>
								<p class="text-justify">
									El paquete de productos y servicios disponibles para la Porcicultura nacional están soportados por <b>VIMIFOS</b> con un excelente servicio técnico en las áreas de nutrición, manejo, sanidad, aseguramiento de calidad, reproducción y análisis de la información y también por asesores externos tanto nacionales como extranjeros.
								</p>
								<br>
							</div>
							<div class="col-md-4">
								<img src="/img/industrias/porcicultura/introduccion.png"  class="pull-right" alt="">
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="productos">
						<div class="row margin-top-15">
							<div class="col-md-8">
								<div class="row margin-top-15">
									<div class="col-md-3">
										<img src="/img/industrias/porcicultura/productos/1.png" class="center-block" alt="">
									</div>
									<div class="col-md-9">
										<p class="text-justify">
											Línea diseñada para satisfacer los requerimientos nutricionales de las primeras semanas de vida, renovando nuestra misión hacia la sustentabilidad y el cuidado del medio ambiente, sin dejar de lado la eficiencia en la producción<br>
											<b class="porcicultura-color">Mejores ganancia de peso e índices de conversión alimenticia</b>
										</p>
									</div>
								</div>
								<div class="row margin-top-15">
									<div class="col-md-3">
										<img src="/img/industrias/porcicultura/productos/2.png" class="center-block" alt="">
									</div>
									<div class="col-md-9">
										<p class="text-justify">
											Concentrados proteicos diseñados para facilitar la elaboración de alimentos terminados para cerdos en su etapa de preiniciación. Línea de alimentación por fases de acuerdo a las primeras etapas de vida. <br>
											<b class="porcicultura-color">Elaborados con los ingredientes con los más altos estándares de calidad.</b>
										</p>
									</div>
								</div>
								<div class="row margin-top-15">
									<div class="col-md-3">
										<img src="/img/industrias/porcicultura/productos/3.png" class="center-block" alt="">
									</div>
									<div class="col-md-9">
										<p class="text-justify">
											Linea que incorpora todos los microingredientes en una dieta, facilita y eficientiza la elaboración de los alimentos balanceados. Diseñado para optimizar la utilización de los aminoácidos sin desperdicio de los recursos nutricionales y económicos.<br>
											<b class="porcicultura-color">Si le metes a Lean Performance le ganas</b>
										</p>
									</div>
								</div>
								<div class="row margin-top-15">
									<div class="col-md-3">
										<img src="/img/industrias/porcicultura/productos/4.png" class="center-block" alt="">
									</div>
									<div class="col-md-9">
										<p class="text-justify">
											Complemento alimenticio para los cerdos en situaciones de stress en las diferentes etapas de producción como el destete, retos infecciosos, canivalecencias, cambios de temperatura y manejos.<br>
											<b class="porcicultura-color">Un escudo protector para mejorar el rendimiento</b>
										</p>
									</div>
								</div>
								<div class="row margin-top-15">
									<div class="col-md-3">
										<img src="/img/industrias/porcicultura/productos/5.png" class="center-block" alt="">
									</div>
									<div class="col-md-9">
										<p class="text-justify">
											Full Blend es una linea de alimentos balanceados con la más completa gama de ingredientes que permite a los cerdos desarrollar su completo potencial.<br>
											<b class="porcicultura-color">Un alimento completo para mejorar tu producción</b>
										</p>
									</div>
								</div>

								<div class="row margin-top-15">
									<div class="col-md-3">
										<img src="/img/industrias/porcicultura/productos/6.png" class="center-block" alt="">
									</div>
									<div class="col-md-9">
										<p class="text-justify">
											Alimento completo peletizado para sementales porcinos, que mantiene la condición corporal del semental en óptimas condiciones, incrementando el líbido y el volumen del esperma, alargando su vida productiva.<br>
											<b class="porcicultura-color">Sementales en optimas condiciones</b>
										</p>
									</div>
								</div>
								
								
								
							</div>
							<div class="col-md-4">
								<img src="/img/industrias/porcicultura/productos.png"  class="img-responsive center-block" alt="">
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="tecnologias">
						<div class="row margin-top-15">
							<div class="col-md-8">
								
								<h5 class="porcicultura-color">TECNOLOGÍAS</h5>
								<br>
								<p class="text-justify porcicultura-color">
									<b>
									Una de las grandes cualidades que diferencian a VIMIFOS es la utilización de tecnología exclusiva aplicada al desarrollo de la nutrición.
									</b>
								</p>

								<li class="porcicultura-color">Vitamina E:</li>
								<p class="text-justify">
									Improtante antioxidante soluble en lípidos-redundante, que proteje a las lipoproteínas y los ácidos grasos insaturados en las membranas celulares. La lucha contra los efectos de los radicales libres derivados del oxígeno y los contaminantes del medio ambiente, la vitamina E reduce la tasa de ataque de los radicales libres sobre los ácidos grasos poliinsaturados en la membrana de fosfolípidos y en otros sitios de importancia biológica.
								</p>

								<img src="/img/industrias/porcicultura/tecnologias-img.png" class="center-block margin-top-15" alt="">
							</div>
							<div class="col-md-4">
								<img src="/img/industrias/porcicultura/tecnologias.png"  class="img-responsive pull-right" alt="">
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="alianzas">
						<div class="row margin-top-15">
							<div class="col-md-8">
								
								<h5 class="porcicultura-color">ALIANZAS</h5>
								<br>
								<p class="text-justify porcicultura-color">
									<b>
									Contamos con alianzas estratégicas que nos ayudan a elaborar productos únicos que ayuden a obtener mayor productividad en los negocios de nuestros clientes.
									</b>
								</p>

								<li class="porcicultura-color">ADM:</li>
								<p class="text-justify">
									Archer Daniels Midland Company es uno de los más grandes procesadores agrícolas del mundo. Fundada en 1902 e incorporada en 1923. ADM tiene su sede en DECATUR, su operación es en todo el mundo a través de sus amplias instalaciones de distribución global. <br><br>
									ADM hace una contribución significativa a la economía mundial y la calidad de vida.
								</p>

								<img src="/img/industrias/porcicultura/adm.png" class="center-block margin-top-15" alt="">
							</div>
							<div class="col-md-4">
								<img src="/img/industrias/porcicultura/alianzas.png"  class="img-responsive pull-right" alt="">
							</div>
						</div>
					</div>
				</div>

			</div>
			

		</div>
	</div>

@endsection



@section('footer')
	@parent
@endsection

@section('scripts')
	
	

@endsection


