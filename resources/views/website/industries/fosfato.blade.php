@extends('layouts.website')

@section('title', 'Page Title')

@section('navbar')
    @parent
@endsection

@section('content')
	
	<div class="container">

		
		<div class="row">
			
			<div class="col-md-12">

				<!-- Tabs login -->
				<ul class="nav nav-tabs tab-fosfato" role="tablist">
					<li role="presentation" class="active">
						<a href="#fosfato" aria-controls="fosfato" role="tab" data-toggle="tab">FOSTATO Y COMERCIALES</a>
					</li>
					<li role="presentation">
						<a href="#introduccion" aria-controls="introduccion" role="tab" data-toggle="tab">INTRODUCCIÓN</a>
					</li>
					<li role="presentation">
						<a href="#productos" aria-controls="productos" role="tab" data-toggle="tab">PRODUCTOS</a>
					</li>
					<li role="presentation">
						<a href="#alianzas" aria-controls="alianzas" role="tab" data-toggle="tab">ALIANZAS</a>
					</li>
				</ul>

				<!-- Contenido de las tabs login -->
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active padding-5 " id="fosfato">
						<div class="row margin-top-15">
							<div class="col-md-12">
								{{-- <img class="center-block" src="/img/industrias/equinos/equinos.jpg" alt=""> --}}
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="introduccion">
						<div class="row margin-top-15">
							<div class="col-md-8">
								<h3 class="fosfato-color">INTRODUCCIÓN</h3> <br>
								<p class="text-justify fosfato-color">
									<b>Fosfato y Comerciales se enfoca de proveer a los fabricantes de alimentos y premezclas con las materias primas de la más alta calidad para ayudar a asegurar el mejor desempeño de los animales.</b> 
								</p>
								<br>
								<p class="text-justify">
									Nuestra cartera de productos incluye las fuentes de fósforo más confiable de la industria: Vimifos 21; así como con una novedosa fuente de energía: Energy Feed. También contamos con representaciones de compañías de prestigio mundial que han confiado en VIMIFOS como la empresa adecuada para hacer llegar sus productos al mercado mexicano.
 									<br><br>
									Con una actitud positiva, y a través del equipo de ventas más reconocido de la industria, nos esforzamos por proveer el mejor servicio, soporte técnico y precio adecuado a nuestros clientes.
									
								</p>
								
								<img src="/img/industrias/fosfato/introduccion-1.png" class="center-block" alt="">
								
							</div>
							<div class="col-md-3 col-md-offset-1">
								<img src="/img/industrias/fosfato/introduccion.png"  class="center-block img-100" alt="">
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="productos">
						<div class="row margin-top-15">
							<div class="col-md-8">
								
								<div class="row margin-top-15">
									<div class="col-md-3">
										<img src="/img/industrias/fosfato/productos/1.png" class="center-block" alt="">
									</div>
									<div class="col-md-9">
										<p class="text-justify">
											Materias primas fabricadas por VIMIFOS con los más altos estándares de calidad y reconocido prestigio en la industria.<br>
											<b class="fosfato-color">Productos de casa.</b>
										</p>
									</div>
								</div>
								<div class="row margin-top-15">
									<div class="col-md-3">
										<img src="/img/industrias/fosfato/productos/2.png" class="center-block" alt="">
									</div>
									<div class="col-md-9">
										<p class="text-justify">
											Productos de uso común, que requieren poco soporte técnico y para los cuales garantizamos calidad, abasto y precio competitivo.<br>
											<b class="fosfato-color">Ingredientes.</b>
										</p>
									</div>
								</div>
								<div class="row margin-top-15">
									<div class="col-md-3">
										<img src="/img/industrias/fosfato/productos/3.png" class="center-block" alt="">
									</div>
									<div class="col-md-9">
										<p class="text-justify">
											Productos de fácil aplicación directa en campo, diseñados para hacer más eficiente el manejo de los animales.  <br>
											<b class="fosfato-color">Soluciones.  </b>
										</p>
									</div>
								</div>		

								<div class="row">
									<div class="col-md-3">
										<img src="/img/industrias/fosfato/productos/4.png" class="center-block" alt="">
									</div>
									<div class="col-md-9">
										<p class="text-justify">
											Siendo una empresa innovadora VIMIFOS pone a disposición de la industria los productos más avanzados, junto con el soporte técnico necesario para su correcta utilización.  <br>
											<b class="fosfato-color">Tecnologías.</b>
										</p>
									</div>
								</div>			
								
								
							</div>
							<div class="col-md-3 col-md-offset-1">
								<img src="/img/industrias/fosfato/productos.png"  class="img-100" alt="">
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="alianzas">
						<div class="row margin-top-15">
							<div class="col-md-8">
								
								<h5 class="fosfato-color">ALIANZAS</h5>
								<br>
								<p class="text-justify fosfato-color">
									<b>
									Gracias a nuestras alianzas estratégicas contamos con una amplia variedad de productos que comercializamos en México.
									</b>
								</p>
								<div class="col-md-10 col-md-offset-1">
									<img src="/img/industrias/fosfato/alianzas-1.png" class="center-block margin-top-15 img-100" alt="">
								</div>
							</div>
							<div class="col-md-3 col-md-offset-1">
								<img src="/img/industrias/fosfato/alianzas.png"  class="img-100" alt="">
							</div>
						</div>
					</div>
				</div>

			</div>
			

		</div>
	</div>

@endsection



@section('footer')
	@parent
@endsection

@section('scripts')
	
	

@endsection


