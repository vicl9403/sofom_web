@extends('layouts.website')

@section('title', 'Page Title')

@section('navbar')
    @parent
@endsection

@section('content')
	
	<div class="container">

		
		<div class="row">
			
			<div class="col-md-12">

				<!-- Tabs login -->
				<ul class="nav nav-tabs tab-camaron" role="tablist">
					<li role="presentation" class="active">
						<a href="#SGI" aria-controls="SGI" role="tab" data-toggle="tab">SGI</a>
					</li>
				</ul>

				<!-- Contenido de las tabs login -->
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active padding-5 " id="SGI">
						<div class="row margin-top-10">
							<div class="col-md-8 camaron-color">
								<h3 class="camaron-color">CERTIFICACIONES</h3> <br>
								<p class="text-justify ">
									<b>NUESTRO SISTEMA DE GESTIÓN INTEGRAL DE CALIDAD SE BASA EN REQUISITOS FSSC 22000.</b> 
								</p>
								<br>
								<p class="text-justify camaron-color">
									En la actualidad los mercados exigen mayor competitividad en las empresas de productos y servicios. Por ello lo primordial es el cumplimiento de los estándares para la satisfacción del consumidor.
									<br><br>
									<b>VIMIFOS</b> es una empresa que trabaja firmemente para lograr la mejora continua logrando los objetivos mediante el Sistema de Gestión Integral, por eso el esfuerzo es constante, para que la CALIDAD este siempre presente para contribuir a los logros de nuestros clientes en rendimiento y productividad.
									<br><br>
									<b>VIMIFOS</b> invierte de manera importante en sistemas de calidad que cubren la certificación de proveedores, materias primas, almacenamiento, embarque y la entrega de productos a nuestros clientes.
									<br><br>
								</p>
								<h4 class="blue">
									En VIMIFOS, la calidad es el resultado de un esfuerzo permanente para alcanzar los compromisos adquiridos.
								</h4>
								
								<br>

								<p>Actualmente contamos con la certificaciones:</p>
								<li>ISO 22000 2013-2016 para las planta PAES en El Salto, Jalisco.</li>
								<li>ISO 22000 2013-2016 para las planta PATCO en Cd. Obregón, Sonora. </li>
								<li>HACCP 2012-2015 para la planta PREMIX en El Salto, Jalisco. </li>
								<li>HACCP 2013-2016 para la planta de FOSFATO en El Salto, Jalisco. </li>
								
								<br>
								
								<h4 class="blue">"VIMIFOS, Siempre un paso adelante"</h4>
							</div>
							<div class="col-md-3 col-md-offset-1">
								<img src="/img/nosotros/certificaciones/sgi.png"  class="img-100" alt="">
							</div>
						</div>
						</div>
					</div>
				</div>

			</div>
			

		</div>
	</div>

@endsection



@section('footer')
	@parent
@endsection

@section('scripts')
	
	

@endsection


