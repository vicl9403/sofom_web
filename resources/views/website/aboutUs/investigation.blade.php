@extends('layouts.website')

@section('title', 'Page Title')

@section('navbar')
    @parent
@endsection

@section('content')
	
	<div class="container">

		
		<div class="row">
			
			<div class="col-md-12">

				<!-- Tabs login -->
				<ul class="nav nav-tabs tab-camaron" role="tablist">
					<li role="presentation" class="active">
						<a href="#investigacion" aria-controls="investigacion" role="tab" data-toggle="tab">INVESTIGACIÓN</a>
					</li>
					<li role="presentation">
						<a href="#adm" aria-controls="adm" role="tab" data-toggle="tab">ADM</a>
					</li>
					<li role="presentation">
						<a href="#acuicultura" aria-controls="acuicultura" role="tab" data-toggle="tab">ACUICULTURA</a>
					</li>
					<li role="presentation">
						<a href="#ciav" aria-controls="ciav" role="tab" data-toggle="tab">CIAV</a>
					</li>
				</ul>

				<!-- Contenido de las tabs login -->
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active padding-5 " id="investigacion">
						<div class="row margin-top-15">
							<div class="col-md-8">
								<h3 class="blue">INVESTIGACIÓN</h3> <br>
								<p class="text-justify ">
									<b>VIMIFOS es una de las empresas de nutrición animal que más invierte en Investigación y Desarrollo tanto en México, como en el extranjero a través de sus alianzas estratégicas.  </b> 
									<br><br>
									El Departamento de Investigación y Desarrollo de VIMIFOS se encarga de diseñar, desarrollar productos mediante un proceso sistemático de evaluación, probar nuevos ingredientes y conceptos nutricionales, con la visión de brindar a sus clientes la mejor solución a sus necesidades. Como parte de nuestra misión, hemos buscado compartir conocimientos relevantes a la industria mediante visitas frecuentes de los más reconocidos expertos mundiales en diversos aspectos de la producción, fomentando la transferencia tecnológica acelerada que ha sido fundamental para el desarrollo sustentable de la industria pecuaria y acuícola nacional.
								</p>
							</div>
							<div class="col-md-4">
								
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="adm">
						<div class="row margin-top-15">
							<div class="col-md-8">
								<h3 class="blue">ADM </h3> 
								
								
								<p class="text-justify">
									Los equipos de Investigación de ADM en Quincy y Decatur, Illinois en conjunto con el Centro de Investigación Randall de ADM (ADM Randall Research Center) han desarrollado relaciones interdependientes efectivas y aplicado estos principios en los proyectos de investigación establecidos. <br>
									Un gran equipo de nutriólogos, técnicos, ingenieros, químicos, microbiólogos y bioquímicos trabajan en conjunto para crear los productos y la tecnología que agregaran valor y beneficios importantes a nuestros clientes en el futuro.
								</p>
								<img src="/img/nosotros/investigacion/adm/adm-1.png" class="img-100" alt="">

								<h3>ADM GANADERÍA</h3>
								<p class="text-justify">
									Desde 1885 la tecnología desarrollada por ADM ha creado productos de la más alta calidad nutricional. VIMIFOS aprovecha esta labor para complementar más de 100 años de investigación, con servicios y apoyos en el rancho para los productores comprometidos en lograr el mayor beneficio posible de los recursos forrajeros, diseñando suplementos ideales para complementar las deficiencias nutricionales básicas. 
								</p>

								<img src="/img/nosotros/investigacion/adm/adm-2.png" class="img-100" alt="">
							</div>
							<div class="col-md-4">
								<img src="/img/nosotros/investigacion/adm/adm.png"  class="img-100" alt="">
							</div>
						</div>
					</div>
					
					<div role="tabpanel" class="tab-pane" id="acuicultura">
						<div class="row margin-top-15">
							<div class="col-md-8">
								<h3>ACUICULTURA</h3> <br><br>								
								<p class="text-justify">
									VIMIFOS mantiene un convenio de colaboración con los principales productores Acuícolas del Noroeste para realizar en sus propias instalaciones PRUEBAS DE CAMPO que se requieren para promover mejoras continuas a los alimentos de acuerdo al desempeño esperado en las condiciones particulares de las granjas en cada región. Estas pruebas nos han permitido lanzar al mercado productos innovadores como Shrimp Starter y Micro Pellet que se han convertido en un parteaguas en la nutrición de camarones en las primeras etapas del cultivo.
								</p> <br><br>

								<h3>INTEGRATED AQUACULTURE</h3>
								<p class="text-justify">
									De la misma forma se trabaja en colaboración con la empresa Integrated Aquaculture International, la cual opera instalaciones de investigación en nutrición y genética de organismos acuáticos en Hawaii, Brunei, India y China. En México se mantienen convenios de colaboración con los principales productores acuícolas para pruebas de campo en sus instalaciones que permitan la mejora continua de las dietas y su adecuación al entorno local. 
								</p>
								<br><br>
								<img src="/img/nosotros/investigacion/acuicultura-1.png"  class="img-100" alt="">
							</div>
							<div class="col-md-4">
								<img src="/img/nosotros/investigacion/acuicultura.png"  class="img-100" alt="">
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="ciav">
						<div class="row margin-top-15">
							<div class="col-md-8">
								
								<h5>CIAV</h5>
								<br>
								<p class="text-justify">
									VIMIFOS es una de las empresas de nutrición animal que más invierte en investigación y desarrollo tanto en México como en el extranjero. Uno de los componentes más importantes para la competitividad es la capacidad para ofrecer soluciones y alternativas de productos y servicios que respondan a la demanda de un mundo en constante cambio. 
									<br><br>
									En el CIAV diseñamos y desarrollamos productos mediante un proceso sistemático de evaluación y prueba de nuevos ingredientes e innovando con conceptos nutricionales, siempre con la visión de proporcionar a sus clientes la mejor solución a sus necesidades. Adicionalmente a este proceso de investigación, se lleva a cabo un trabajo permanente de optimización del desempeño y costo de los productos, buscando con esto maximizar la rentabilidad de las operaciones de los clientes. 
								</p>
								<div class="col-md-10 col-md-offset-1">
									<img src="/img/nosotros/investigacion/ciav/ciav-1.png" class="center-block margin-top-15 img-100" alt="">
								</div>
							</div>
							<div class="col-md-3 col-md-offset-1">
								<img src="/img/nosotros/investigacion/ciav/ciav.png" class="center-block margin-top-15 img-100" alt="">
							</div>
						</div>
					</div>
				</div>

			</div>
			

		</div>
	</div>

@endsection



@section('footer')
	@parent
@endsection

@section('scripts')
	
	

@endsection


