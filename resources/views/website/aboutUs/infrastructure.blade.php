@extends('layouts.website')

@section('title', 'Page Title')

@section('navbar')
    @parent
@endsection

@section('content')
	
	<div class="container">

		
		<div class="row">
			
			<div class="col-md-12">

				<!-- Tabs login -->
				<ul class="nav nav-tabs tab-camaron" role="tablist">
					<li role="presentation" class="active">
						<a href="#infraestructura" aria-controls="infraestructura" role="tab" data-toggle="tab">INFRAESTRUCTURA</a>
					</li>
					<li role="presentation">
						<a href="#plantas-y-almacenes" aria-controls="plantas-y-almacenes" role="tab" data-toggle="tab">PLANTAS Y ALMACENES</a>
					</li>
					<li role="presentation">
						<a href="#tecnologia" aria-controls="tecnologia" role="tab" data-toggle="tab">TECNOLOGÍA Y CALIDAD</a>
					</li>
					<li role="presentation">
						<a href="#alianzas" aria-controls="alianzas" role="tab" data-toggle="tab">ALIANZAS</a>
					</li>
				</ul>

				<!-- Contenido de las tabs login -->
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active padding-5 " id="infraestructura">
						<div class="row margin-top-15">
							<div class="col-md-12">
								{{-- <img class="center-block" src="/img/industrias/avicultura/tab-1.png" alt=""> --}}
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="plantas-y-almacenes">
						<div class="row margin-top-15">
							<div class="col-md-8">
								<h3 class="blue">PLANTAS Y ALMACENES</h3> <br>
								<p class="text-justify blue">
									<b>VIMIFOS cuenta con  plantas  de  producción modernas, equipadas  con  tecnología de punta y estratégicamente localizadas para brindar una atención y servicio acorde a los requerimientos de la industria. </b> 
								</p>
								<br>
								<p class="text-justify">
									Cuenta con una de las mejores infraestructuras para la elaboración de alimento  en  América Latina. Actualmente la planta de producción PREMIX ubicada en El Salto, Jalisco, es la más grande de México por su capacidad de producción en premezclas
									<br><br>
									<span class="blue">Almacenes:</span>
									<br>
									Hermosillo, Culiacán, Monterrey y Tehuacán. 
									<br><br>
									<span class="blue">Plantas:</span>
									<br>
									Cd. Obregón: Ácidos Grasos, Premix, Alimentos Especializados y Convencionales. <br>
									Guadalajara: Ácidos Grasos, Premix, Fosfato, Especialidades.
									<br><br>

									<span class="blue">Ubicación plantas y almacenes:</span>
								</p>
								
								<img src="/img/nosotros/infraestructura/mapa.png"  class="img-100" alt="">
								
							</div>
							<div class="col-md-3 col-md-offset-1">
								<img src="/img/nosotros/infraestructura/plantas.png"  class="img-100" alt="">
							</div>
						</div>
					</div>
					
					<div role="tabpanel" class="tab-pane" id="tecnologia">
						<div class="row margin-top-15">
							<div class="col-md-8">								
							</div>
							<div class="col-md-4">
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="alianzas">
						<div class="row margin-top-15">
							<div class="col-md-8">
								
								<h5 class="blue">ALIANZAS</h5>
								<br>
								<p class="text-justify blue">
									<b>
									Contamos con alianzas estratégicas que nos apoyan a elaborar productos únicos que ayudan a obtener una mayor productividad en los negocios de nuestros clientes.
									</b>
								</p>
								<div class="col-md-10 col-md-offset-1">
									<img src="/img/industrias/fosfato/alianzas-1.png" class="center-block margin-top-15 img-100" alt="">
								</div>
							</div>
							<div class="col-md-3 col-md-offset-1">
								<img src="/img/industrias/fosfato/alianzas.png"  class="img-100" alt="">
							</div>
						</div>
					</div>
				</div>

			</div>
			

		</div>
	</div>

@endsection



@section('footer')
	@parent
@endsection

@section('scripts')
	
	

@endsection


