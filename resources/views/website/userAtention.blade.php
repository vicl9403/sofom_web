@extends('layouts.website')

@section('title', 'Page Title')

@section('navbar')
    @parent
@endsection

@section('content')

    <div class="container">


        <div class="row">

            <div class="col-md-12">

                <h4>ATENCIÓN A USUARIOS</h4>

                <h4 class="text-center">
                    <b>Corporativo Financiero Vimifos S. A. de C. V. SOFOM E. N. R.</b> <br>
                    UNE de Atención a Usuarios
                </h4>

                <p>
                    En caso de alguna consulta, reclamación o aclaración, podrá presentarla
                    en la UNE de Atención a Usuarios, la que dará respuesta en un plazo no mayor a 30 días hábiles.
                    <br><br>
                    Titular de la UNE: Guillermo de Jesús Richaud Villalvazo <br>
                    Domicilio: Av. Lázaro Cárdenas 3430 Piso 3 Int 303 y 304, Col. Jardines de los Arcos, Guadalajara, Jalisco CP 44500
                    <br>
                    Horario de atención: Lunes a Viernes de 9 am -  6 pm
                    <br>
                    Teléfono y correo electrónico: (33) 38802400 y fax (33) 38802407 jrichaud@vimifos.com
                    <br><br>
                    Encargados Regionales:
                </p>

                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Entidades Federativas / Municipios</th>
                            <th>Encargado (s)</th>
                            <th>Domicilio (s)</th>
                            <th>Teléfono (s) y correo electrónico (s)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Jalisco</td>
                            <td>Guillermo de Jesús Richaud Villalvazo</td>
                            <td>Av. Lázaro Cárdenas 3430 Piso 3 Int 303 y 304, Col. Jardines de los Arcos, Guadalajara, Jalisco CP 44500 </td>
                            <td>tel (33) 38802400 fax (33) 38802407 jrichaud@vimifos.com</td>
                        </tr>
                        <tr>
                            <td>Sonora</td>
                            <td>Carlos Rafael Romero Acuña	</td>
                            <td>Carretera Internacional México 15 km 13, Col. Centauro del Norte, Cajeme, Sonora CP 85205</td>
                            <td>tel (64) 44109500 fax (64) 44109500 cromero@vimifos.com	</td>
                        </tr>
                    </tbody>
                </table>

                <p>El horario de atención de los Encargados Regionales es: Lunes a Viernes  de  9 am  -  6 pm
                </p>

            </div>


        </div>
    </div>

@endsection



@section('footer')
    @parent
@endsection

@section('scripts')



@endsection


