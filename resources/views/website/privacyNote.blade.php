@extends('layouts.website')

@section('title', 'Page Title')

@section('navbar')
    @parent
@endsection

@section('content')

    <div class="container">


        <div class="row">

            <div class="col-md-12">

                <h4 class="blue">Aviso de Privacidad</h4>

                <br>

                <p>

                    En cumplimiento con lo establecido en la Ley Federal de Protección de Datos
                    Personales en Posesión de Particulares se emite el siguiente aviso de privacidad:
                    <br><br>

                    La empresa VIMIFOS, S.A. de C.V. y sus filiales y asociadas con domicilio
                    en calle 4 número 10500, Parque Industrial el Salto en el Salto, Jalisco,
                    y en Av. Lázaro Cárdenas 3430 piso 3 int. 303-304, Jardines de los Arcos
                    en Guadalajara, Jalisco son responsables de recabar sus datos personales,
                    del uso que se da a los mismos y de su protección. <br><br>

                    Con base en la Ley Federal de Protección de Datos Personales en Posesión de
                    Particulares, Grupo VIMIFOS reconoce que es importante asegurarle la debida
                    Privacidad y la Seguridad de su información de acuerdo con las leyes vigentes,
                    por lo cual estamos comprometidos a resguardar su información personal de
                    acuerdo con las prácticas legales, administrativas y conforme a estándares
                    de seguridad informática propios de la industria. Así mismo, nos responsabilizaremos
                    a no comercializar, alquilar, compartir o divulgar su información personal a
                    terceros con fines ilícitos o contrarios a los de su titular, salvo en los casos
                    en que expresamente el titular lo autorice. <br><br>

                    De acuerdo a lo anterior, el presente “Aviso de Privacidad” se aplica a toda
                    la información personal recopilada por Grupo VIMIFOS incluyendo sus empresas
                    afiliadas y subsidiarias, así como de terceros con los que celebre o vaya a
                    celebrar relación contractual, a efecto de brindarle servicios personalizados,
                    contando siempre con los niveles de seguridad exigidos por Ley Federal de
                    Protección de Datos Personales en Posesión de los Particulares. Por lo antes
                    mencionado, y al estar de acuerdo con el presente “Aviso de Privacidad”,
                    usted otorga su consentimiento a Grupo VIMIFOS para recopilar y utilizar sus
                    datos personales para los fines que más adelante se especifican. Le recordamos
                    que en materia de Protección de Datos Personales, usted podrá ejercer sus
                    derechos denominados “ARCO” de acuerdo a lo siguiente: <br><br>

                    <ul>
                        <li>
                            Acceso: Podrá elegir la manera de comunicarse con nosotros a través de
                            los medios que más adelante se mencionan para saber si Grupo VIMIFOS
                            cuenta con sus datos personales y los detalles de los mismos.
                        </li>
                        <li>
                            Rectificación: Usted podrá solicitarnos que cualquiera de sus datos
                            sea corregido, en caso de que tengamos registrado alguno erróneamente
                            o que el mismo haya sido modificado.
                        </li>
                        <li>
                            Cancelación: Podrá pedir que cancelemos o demos de baja sus datos
                            siempre y cuando exista una causa que justifique dicha acción y no
                            tenga obligación pendiente de cubrir por parte o a favor de Grupo VIMIFOS.
                        </li>
                        <li>
                            Oposición: En caso de que usted ya no tenga relación u obligación
                            legal alguna con nosotros, puede hacer uso de este derecho, no
                            compartiendo dato alguno.
                        </li>
                    </ul>
                    <br><br>
                    Dependiendo de la relación que tengamos con usted, la información que recabamos
                consiste en nombre, edad, estado civil, domicilio, teléfono, correo electrónico,
                lugar y fecha de nacimiento (también de cónyuge e hijos), registro federal de contribuyentes,
                cedula única de registro de población, número de seguridad social, cédula profesional,
                pasaporte y/o visa, cuenta de institución bancaria, clave bancaria estandarizada y demás
                datos financieros, escolaridad, empleos anteriores, tipo de sangre, alergias, enfermedades
                crónicas, peso, estatura, referencias comerciales, y demás datos de la misma naturaleza. <br><br>

                    Estos datos son proporcionados por usted, según corresponda, a través de los departamentos
                recursos humanos y servicios médicos, compras, ventas, cuentas por cobrar, cuentas por pagar,
                lo anterior como parte de la operación contractual que se tiene o se pretenda tener con usted,
                para los fines específicos que más adelante se precisan. Estos datos son utilizados para cumplir
                con los lineamientos fiscales y legales derivados de la relación que tenemos con usted, como lo
                son -dependiendo del caso que se trate- selección y contratación de personal, seguimiento de medidas
                de seguridad, avisos en caso de emergencia, otorgamiento de prestaciones como empleado, asignación
                de funciones y/o responsabilidades, aceptación de clientes, evaluación de proveedores, facturación,
                cobranza, pagos, otorgamiento de créditos, solicitud de pedidos, envío de mercancía e información
                de productos y cambios a los mismos, entre otros.
                    <br><br>
                    Le informamos que sus datos personales únicamente son transferidos y tratados por nuestras
                empresas filiales o subsidiarias, o algún tercero debidamente contratado con cláusulas de
                confidencialidad de información y de protección de datos personales. Lo anterior para los
                fines mencionados en el párrafo que precede. En caso de que tenga alguna pregunta o desee
                conocer los datos de su persona que constan en nuestras bases de datos, actualizarlos o
                rectificarlos (en caso de ser erróneos), o ejercer el derecho de retiro o bloqueo total o parcial
                de los mismos, puede contactarnos en Grupo VIMIFOS a través del responsable de tratamiento de
                datos personales en el correo electrónico contacto@vimifos.com


                </p>

            </div>


        </div>
    </div>

@endsection



@section('footer')
    @parent
@endsection

@section('scripts')



@endsection


