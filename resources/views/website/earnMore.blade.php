@extends('layouts.website')

@section('title', 'Page Title')

@section('navbar')
    @parent
@endsection

@section('content')

    <div class="container">


        <div class="row">

            <div class="col-md-12">


                @if( isset($success) )

                    <div class="alert alert-success">
                        <strong>Éxito!</strong> Gracias por ponerte en contacto con nosotros.
                    </div>

                @endif
                <!-- Tabs login -->
                <ul class="nav nav-tabs " role="tablist">
                    <li role="presentation" class="{{ $contact ? '' : 'active' }}">
                        <a href="#nosotros" aria-controls="nosotros" role="tab" data-toggle="tab">
                            GANA MÁS
                        </a>
                    </li>
                    <li role="presentation" class="{{ ! $contact ? '' : 'active' }}">
                        <a href="#mision-y-vision" aria-controls="mision-y-vision" role="tab" data-toggle="tab">
                            COMIENZA YA!
                        </a>
                    </li>
                </ul>

                <!-- Contenido de las tabs login -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane {{ $contact ? '' : 'active' }}" id="nosotros">
                        <div class="row margin-top-15">
                            <div class="col-md-12">

                                <div id="earn-more-carousel" class="carousel slide" data-ride="carousel">
                                    <!-- Indicators -->
                                    <ol class="carousel-indicators">
                                        <li data-target="#earn-more-carousel" data-slide-to="0" class="active"></li>
                                        <li data-target="#earn-more-carousel" data-slide-to="1"></li>
                                        <li data-target="#earn-more-carousel" data-slide-to="2"></li>
                                        <li data-target="#earn-more-carousel" data-slide-to="3"></li>
                                        <li data-target="#earn-more-carousel" data-slide-to="4"></li>
                                        <li data-target="#earn-more-carousel" data-slide-to="5"></li>
                                    </ol>

                                    <!-- Wrapper for slides -->
                                    <div class="carousel-inner" role="listbox">
                                        <div class="item active">
                                            <img class="center" src="img/slide/9.jpg" alt="...">
                                        </div>
                                        <div class="item">
                                            <img class="center" src="img/slide/8.jpg" alt="">
                                        </div>
                                        <div class="item">
                                            <img class="center" src="img/slide/7.jpg" alt="">
                                        </div>
                                        <div class="item">
                                            <img class="center" src="img/slide/6.jpg" alt="">
                                        </div>
                                        <div class="item">
                                            <img class="center" src="img/slide/5.jpg" alt="">
                                        </div>
                                        <div class="item">
                                            <img class="center" src="img/slide/4.jpg" alt="">
                                        </div>
                                    </div>

                                    <!-- Controls -->
                                    <a class="left carousel-control" href="#earn-more-carousel" role="button" data-slide="prev">
                                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="right carousel-control" href="#earn-more-carousel" role="button" data-slide="next">
                                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>


                            </div>

                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane {{ ! $contact ? '' : 'active' }}" id="mision-y-vision">
                        <div class="row margin-top-15">
                            <div class="col-md-8">
                                <h4 class="blue">
                                    Necesitas más información acerca de nuestros soluciones, envíanos un mensaje.
                                </h4>
                                <h4 class="blue">"Somos una organización con espíritu emprendedor y de servicio,
                                    que contribuye a la prosperidad de nuestros clientes."</h4>
                                <br>

                                <form action="/gana-mas"
                                      method="post"
                                >

                                    {{ csrf_field() }}
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-3">
                                                <label for="" class="">Nombre: </label>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" name="name">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row margin-5">
                                        <div class="form-group">
                                            <div class="col-md-3">
                                                <label for="" class="">Localidad: </label>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" name="location">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row margin-5">
                                        <div class="form-group">
                                            <div class="col-md-3">
                                                <label for="" class="">Teléfono: </label>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" name="phone">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row margin-5">
                                        <div class="form-group">
                                            <div class="col-md-3">
                                                <label for="" class="">Email: </label>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="email" class="form-control" name="email">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row margin-5">
                                        <div class="form-group">
                                            <div class="col-md-3">
                                                <label for="" class="">Comentarios: </label>
                                            </div>
                                            <div class="col-md-6">
                                                <textarea name="comments" id="" cols="30" rows="5" class="form-control"></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row margin-5">
                                        <div class="col-md-4 col-md-offset-4">
                                            <button class="btn btn-default btn-block">Enviar</button>
                                        </div>
                                    </div>

                                </form>

                                <p>
                                    <b> Atención Directa:</b> <br>
                                    <b>
                                        Av. Lázaro Cárdenas #3430, Piso 3 Int. 303-304, Col. Jardines de Los Arcos,
                                        C.P. 44500, Guadalajara, Jalisco. Tel: 01 (33) 3880 2405, 01 800 00 76 365
                                    </b>
                                </p>

                                <p>
                                    <b>AVISO DE PRIVACIDAD</b>
                                    En cumplimiento con lo establecido en la Ley Federal de Protección de Datos Personales
                                    en Posesión de Particulares se emite el siguiente aviso de privacidad: La empresa VIMIFOS,
                                    S.A. de C.V. y sus filiales y asociadas con domicilio en calle 4 número 10500, Parque
                                    Industrial el Salto, en  el Salto, Jalisco, y en AV. Lázaro Cárdenas 3430 piso 3 int. 303-304,
                                    Jardines de los Arcos en Guadalajara, Jalisco utilizarán los datos personales aquí recabados
                                    para fines de selección de personal, estadísticos, de promoción, de publicidad, mercadológicos
                                    o de prospección comercial. Si requiere mayor información acerca del tratamiento y los derechos
                                    que puede hacer valer en relación con sus datos personales, puede acceder al aviso de privacidad
                                    completo y los cambios al mismo. Disponibles a través de nuestra página web en la siguiente
                                    dirección: <a href="/aviso-de-privacidad">http://www.sofom.vimifos.com/avisodeprivacidad</a>
                                </p>

                            </div>
                            <div class="col-md-4">
                                <img src="/img/ganamas/banner-comienza.jpg"  class="img-responsive img-100" alt="">
                            </div>
                        </div>
                    </div>
                </div>

            </div>


        </div>
    </div>

@endsection



@section('footer')
    @parent
@endsection

@section('scripts')



@endsection


