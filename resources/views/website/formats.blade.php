@extends('layouts.website')

@section('title', 'Page Title')

@section('navbar')
    @parent
@endsection

@section('content')

    <div class="container">


        <div class="row">

            <div class="col-md-12">

                <!-- Tabs login -->
                <ul class="nav nav-tabs " role="tablist">
                    <li role="presentation" class="active">
                        <a href="#nosotros" aria-controls="nosotros" role="tab" data-toggle="tab">
                            FORMATOS
                        </a>
                    </li>
                </ul>

                <!-- Contenido de las tabs login -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active padding-5 " id="nosotros">
                        <div class="row margin-top-15">
                            <div class="col-md-8">

                                <p>
                                    Formatos <br>
                                    Podrás buscar y descargar tus archivos en línea que necesites para los trámites correspondietnes de una forma sencilla.
                                </p>

                                <ul>
                                    <li>Descarga rápido</li>
                                    <li>Siempre disponible</li>
                                    <li>Archivos seguros</li>
                                </ul>

                                <p class="text-center blue">DESCARGAS DE ARCHIVOS!</p>

                            </div>
                            <div class="col-md-4">
                                <img src="/img/ganamas/banner-formatos.jpg"  class="img-responsive img-100" alt="">
                            </div>

                        </div>
                    </div>
                </div>

            </div>


        </div>
    </div>

@endsection



@section('footer')
    @parent
@endsection

@section('scripts')



@endsection


