@extends('layouts.app')

@section('content')
<div class="container flex-center position-ref full-height">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-primary ">
                <div class="panel-heading text-center top">Formulario de Registro</div>
                <div class="panel-body">


                    <form class="" role="form" method="POST" action="{{ url('/register') }}">
                        {{ csrf_field() }}

                        
                        <div class="form-group form-md-line-input form-md-floating-label has-info {{ $errors->has('name') ? ' has-error' : '' }}">
                            <div class="input-group left-addon">
                                <span class="input-group-addon">
                                    <i class="fa fa-user"></i>
                                </span>
                                <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" required>
                                <label for="name">Nombre</label>
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group form-md-line-input has-info form-md-floating-label {{ $errors->has('email') ? ' has-error' : '' }}">
                            <div class="input-group left-addon">
                                <span class="input-group-addon">
                                    <i class="fa fa-envelope"></i>
                                </span>
                                <input type="email" class="form-control" id="email" name="email" required>
                                <label for="email">Email</label>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group form-md-line-input has-info form-md-floating-label {{ $errors->has('password') ? ' has-error' : '' }}">
                            <div class="input-group left-addon">
                                <span class="input-group-addon">
                                    <i class="fa fa-lock"></i>
                                </span>
                                <input type="password" class="form-control" id="password" name="password" required>
                                <label for="password">Contraseña</label>
                            
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif

                            </div>
                        </div>

                        <div class="form-group form-md-line-input has-info form-md-floating-label">
                            <div class="input-group left-addon">
                                <span class="input-group-addon">
                                    <i class="fa fa-lock"></i>
                                </span>
                                <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" required>
                                <label for="password_confirmation">Repetir contraseña</label>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary pull-right">
                                    Registrarme
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
