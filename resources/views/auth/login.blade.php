@extends('layouts.app')

@section('page-styles')
    <link href="/assets/pages/css/login-5.min.css" rel="stylesheet" type="text/css" />
@endsection

@section('content')

    <!-- BEGIN : LOGIN PAGE 5-2 -->
    <div class="user-login-5">
        <div class="row bs-reset">
            <div class="col-md-6 login-container bs-reset">
                <img class="login-logo login-6" src="../assets/pages/img/login/login-invert.png" />
                <img class="login-logo login-6 " src="img/logo.jpg" />
                <div class="login-content">
                    <h1>Vimifos Admin Login</h1>
                    <p> Lorem ipsum dolor sit amet, coectetuer adipiscing elit sed diam nonummy et nibh euismod aliquam erat volutpat. Lorem ipsum dolor sit amet, coectetuer adipiscing. </p>
                    <form action="{{ url('/login') }}" class="login-form" method="post">
                        {{ csrf_field() }}

                        

                        <div class="alert alert-danger display-hide">
                            <button class="close" data-close="alert"></button>
                            <span>Escribe un usuario y contraseña </span>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                <input id="email" type="email" class="form-control form-control-solid placeholder-no-fix form-group" autocomplete="off" placeholder="Correo electrónico" name="email" value="{{ old('email') }}" required autofocus>
                            </div>
                            <div class="col-xs-6">
                                <input class="form-control form-control-solid placeholder-no-fix form-group" type="password" autocomplete="off" placeholder="Contraseña" id="password" name="password" required/>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="rem-password">
                                    <p>Recordar
                                        <input type="checkbox" name="remember" class="rem-checkbox" />
                                    </p>
                                </div>
                            </div>
                            <div class="col-sm-8 text-right">
                                <div class="forgot-password">
                                    <a href="javascript:;" id="forget-password" class="forget-password">Olvidaste tu contraseña?</a>
                                </div>
                                <button class="btn blue" type="submit">Iniciar sesión</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 text-right margin-top-15">
                                <a href="/register">¿No tienes cuenta? Regístrate</a>
                            </div>
                        </div>
                    </form>
                    <!-- BEGIN FORGOT PASSWORD FORM -->
                    <form class="forget-form" action="{{ url('/password/email') }}" method="post">
                        {{ csrf_field() }}
                        <h3 class="font-green">Olvidaste tu contraseña ?</h3>
                        <p> Ingresa tu dirección de e-mail para recuperar tu contraseña. </p>
                        <div class="form-group">
                            <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email" /> </div>
                        <div class="form-actions">
                            <button type="button" id="back-btn" class="btn grey btn-default">Regresar</button>
                            <button type="submit" class="btn blue btn-success uppercase pull-right">Enviar</button>
                        </div>
                    </form>
                    <!-- END FORGOT PASSWORD FORM -->
                </div>
                <div class="login-footer hidden">
                    <div class="row bs-reset">
                        <div class="col-xs-5 bs-reset">
                            <ul class="login-social">
                                <li>
                                    <a href="javascript:;">
                                        <i class="icon-social-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:;">
                                        <i class="icon-social-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:;">
                                        <i class="icon-social-dribbble"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-xs-7 bs-reset">
                            <div class="login-copyright text-right">
                                <p>Copyright &copy; Victor López 2015</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 bs-reset">
                <div class="login-bg"> </div>
            </div>
        </div>
    </div>
    <!-- END : LOGIN PAGE 5-2 -->
    
@endsection

@section('page-scripts')
    <script src="/assets/pages/scripts/login-5.min.js" type="text/javascript"></script>
@endsection
