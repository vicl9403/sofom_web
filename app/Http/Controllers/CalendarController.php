<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\Room;
use Illuminate\Http\Request;

class CalendarController extends Controller
{
    public function index($id = null)
    {
    	$rooms = Room::all();

    	if($id == null)
    		$events = Event::get();
    	else
    		$events = Event::where( ['room_id' => $id] )->get();

    	$room = Room::find($id);

    	return view('admin.calendar.indexCalendar',compact('rooms','room','events'));
    }
    public function store(Request $request)
    {


    	
    }
}
