<?php

namespace App\Http\Controllers;

use App\Models\Room;
use Illuminate\Http\Request;

use App\Http\Requests\StoreRoom;

class RoomController extends Controller
{
	public function index()
	{
		$rooms = Room::all();
		return view('admin.rooms.indexRooms', compact('rooms'));
	}
	public function create()
	{
		return view('admin.rooms.createRoom');
	}
    public function store(StoreRoom $request)
    {
    	Room::create($request->all());
    	$request->session()->flash('status', 'Sala creada exitosamente');
    	return redirect('/admin/salas');
    }
}
