<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreEvent extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'     => 'required',
            'room_id'   => 'required',
            'start'     => 'required',
            'end'       => 'required',
        ];
    }

    public function messages()
    {
        return [
            'title.required'    => 'El título es requerido',
            'room_id.required'  => 'El campo sala es requerido',
            'start.required'    => 'La fecha de inicio es requerida',
            'end.required'      => 'La fecha de fin es requerida',
        ];
    }
}
